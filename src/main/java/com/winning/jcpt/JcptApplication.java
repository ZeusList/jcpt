package com.winning.jcpt;

import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 启动类继承SpringBootServletInitializer实现configure
 */
@SpringBootApplication
@EnableAutoConfiguration
public class JcptApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(JcptApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

}
