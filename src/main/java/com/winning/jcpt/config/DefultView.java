package com.winning.jcpt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class DefultView extends WebMvcConfigurerAdapter{

    /**
     * @Author zhanglei
     * @Description 设置项目默认访问路径
     * @Date 8:58 2019/3/1
     * @Param [registry]
     * @return void
     **/
    @Override
    public void addViewControllers(ViewControllerRegistry registry)
    {
        //设置登录页
        registry.addViewController("/").setViewName("forward:/html/login.html");
        //设置优先级为最高
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        super.addViewControllers(registry);
    }
}
