package com.winning.jcpt.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/test")
@Api(value = "测试Swagger")
public class testDemoController {

    @RequestMapping(value = "/showHelloWord",method = RequestMethod.GET)
    @ApiOperation(value = "输出Hello World")
    public void showHelloWord(){
        System.out.println("Hello World");
    }


    @RequestMapping(value = "/{userName}", method = RequestMethod.GET)
    @ResponseBody
    @ApiImplicitParam(paramType = "query",name= "username" ,value = "用户名",dataType = "string")
    @ApiOperation(value = "通过用户名称，输出一句问候", notes="返回问候语")
    public String sayHelloByName(@PathVariable String userName) {
        return userName + ",Hello World";
    }

}
